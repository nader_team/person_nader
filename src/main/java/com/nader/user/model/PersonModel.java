package com.nader.user.model;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.Date;
@Entity
@Table(name = "persons")
@EntityListeners(AuditingEntityListener.class)
public class PersonModel {
    @Id
    @Autowired
    @GeneratedValue(strategy = GenerationType.AUTO)

    private Integer person_id;
    private Integer user_id;
    private String userName;
    private String password;
    private String firstName;
    private String lastName;
    private Date createdDate;
    private Date updatedDate;
    private String createdBy;
    private String updatedby;
    @NotNull
    @Min(8)
    private String dateOfBirth;


    public PersonModel() {
        super();
    }

    public Integer getUser_id() {    return user_id;    }
    public void setUser_id(Integer user_id) {        this.user_id = user_id;    }

    public String getUserName() {
        return userName;
    }
    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getPerson_id() {        return person_id;    }
    public void setPerson_id(Integer person_id) {       this.person_id = person_id;    }

    public String getFirstName() {
        return firstName;
    }
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @CreatedDate
    @Temporal(TemporalType.TIMESTAMP)
    public Date getCreatedDate() {
        return createdDate;
    }
    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    @CreatedBy
    public String getCreatedBy() {
        return createdBy;
    }
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @LastModifiedDate
    @Temporal(TemporalType.TIMESTAMP)
    public Date getUpdatedDate() {
        return updatedDate;
    }
    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    @LastModifiedBy
    public String getUpdatedby() {
        return updatedby;
    }
    public void setUpdatedby(String updatedby) {
        this.updatedby = updatedby;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }
    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }
}
