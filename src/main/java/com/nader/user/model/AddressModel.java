package com.nader.user.model;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import javax.persistence.*;
import java.util.Date;
@Entity
@Table(name = "address")
@EntityListeners(AuditingEntityListener.class)
public class AddressModel {
    @Id
    @Autowired
    @GeneratedValue(strategy = GenerationType.AUTO)

     private Integer address_id;
     private String addressLine;
     private String zipCode;
      private String country;
     private Date createdDate;
     private Date updatedDate;
     private String createdBy;
     private String updatedBy;

    public AddressModel() {
        super();
    }

    public Integer getAddress_id() {      return address_id;    }
    public void setAddress_id(Integer address_id) {        this.address_id = address_id;    }

    public String getAddressLine() {
        return addressLine;
    }
    public void setAddressLine(String addressLine) {
        this.addressLine = addressLine;
    }

    public String getZipCode() {
        return zipCode;
    }
    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getCountry() {
        return country;
    }
    public void setCountry(String country) {
        this.country = country;
    }

    @CreatedDate
    @Temporal(TemporalType.TIMESTAMP)
    public Date getCreatedDate() {   return createdDate;    }
    public void setCreatedDate(Date createdDate) {        this.createdDate = createdDate;    }

    @LastModifiedDate
    @Temporal(TemporalType.TIMESTAMP)
    public Date getUpdatedDate() {        return updatedDate;    }
    public void setUpdatedDate(Date updatedDate) {       this.updatedDate = updatedDate;    }

    @CreatedBy
    public String getCreatedBy() {        return createdBy;    }
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @LastModifiedBy
    public String getUpdatedBy() {
        return updatedBy;
    }
    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }
}
