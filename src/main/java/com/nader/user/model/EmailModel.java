package com.nader.user.model;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import javax.persistence.*;
import javax.validation.constraints.Pattern;
import java.util.Date;
@Entity
@Table(name = "emails")
@EntityListeners(AuditingEntityListener.class)
public class EmailModel {
    @Id
    @Autowired
    @GeneratedValue(strategy=GenerationType.AUTO)

    private Integer email_id;
    @Pattern(regexp = ".+@.+\\.[a-z]+")
    private String emailValue;
    private Date createdDate;
    private Date updatedDate;
    private String createdBy;
    private String updatedBy;

    public EmailModel() {
        super();
    }

    public Integer getEmail_id() {
        return email_id;
    }
    public void setEmail_id(Integer email_id) {
        this.email_id = email_id;
    }

    public String getEmailValue() {    return emailValue;    }
    public void setEmailValue(String emailValue) {
        this.emailValue = emailValue;
    }

    @CreatedDate
    @Temporal(TemporalType.TIMESTAMP)
    public Date getCreatedDate() { return createdDate;    }
    public void setCreatedDate(Date createdDate) { this.createdDate = createdDate;    }

    @LastModifiedDate
    @Temporal(TemporalType.TIMESTAMP)
    public Date getUpdatedDate() {        return updatedDate;    }
    public void setUpdatedDate(Date updatedDate) {        this.updatedDate = updatedDate;    }

    @CreatedBy
    public String getCreatedBy() {        return createdBy;    }
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @LastModifiedBy
    public String getUpdatedBy() {
        return updatedBy;
    }
    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }
}
