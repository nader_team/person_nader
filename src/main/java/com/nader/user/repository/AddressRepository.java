package com.nader.user.repository;

import com.nader.user.model.AddressModel;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
@Repository
public interface AddressRepository extends CrudRepository<AddressModel, Integer> {
}
