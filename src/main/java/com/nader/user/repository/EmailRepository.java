package com.nader.user.repository;

import com.nader.user.model.EmailModel;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
@Repository
     public interface EmailRepository extends CrudRepository<EmailModel, Integer> {
}
