package com.nader.user.repository;

 import com.nader.user.model.PersonModel;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface PersonsRepository extends CrudRepository<PersonModel, Integer>
{
    List<PersonModel> getByFirstName(String firstName);
    List<PersonModel> getByUserName(String userName);
}
