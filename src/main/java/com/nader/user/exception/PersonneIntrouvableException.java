package com.nader.user.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;
@ResponseStatus(HttpStatus.NOT_FOUND)
public class PersonneIntrouvableException extends RuntimeException {
    public PersonneIntrouvableException(String s) {
        super("La personne avec " +s+ " n'exite pas ");
   }
}
