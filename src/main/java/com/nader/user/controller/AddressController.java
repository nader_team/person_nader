package com.nader.user.controller;

import com.nader.user.exception.PersonneIntrouvableException;
import com.nader.user.model.AddressModel;
import com.nader.user.repository.AddressRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Optional;
@RestController
@RequestMapping("/api/v1/persons/{person_id}/address")
public class AddressController {
    @Autowired
    private AddressRepository addressRepository;

    @PostMapping("/")
    @ResponseBody
    public AddressModel addAddress(@Valid @RequestBody AddressModel addressModel)
    {
        addressRepository.save(addressModel);
        return addressModel;
    }

    @GetMapping("/")
    @ResponseBody
    public Iterable<AddressModel> viewAllAddress()
    {
        return addressRepository.findAll();
    }

    @GetMapping("/{address_id}")
    @ResponseBody
    public AddressModel viewByAddressId(@PathVariable Integer address_id)
    {
        Optional<AddressModel> opt = addressRepository.findById(address_id);
        return opt.orElseThrow(() -> new PersonneIntrouvableException("l'addressId = " + address_id));
    }

    @DeleteMapping("/{address_id}")
    @ResponseBody
    public ResponseEntity<Object> deleteAddress(@Valid @PathVariable Integer address_id)
    {
        addressRepository.deleteById(address_id);
        return ResponseEntity.noContent().build();
    }

    @PutMapping("/{address_id}")
    @ResponseBody
    public ResponseEntity<Object> editAddress(@Valid @RequestBody AddressModel addressModel, @PathVariable Integer address_id) {
        Optional<AddressModel> rep = addressRepository.findById(address_id);
        return rep.map( p -> {

            addressModel.setAddress_id(address_id);
            addressRepository.save(addressModel);
            return ResponseEntity.status(HttpStatus.CREATED).build();}
        )
                .orElse(ResponseEntity.notFound().build());}
}



