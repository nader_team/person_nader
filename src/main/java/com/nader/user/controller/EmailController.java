package com.nader.user.controller;
import com.nader.user.exception.PersonneIntrouvableException;
import com.nader.user.model.EmailModel;
import com.nader.user.repository.EmailRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.Optional;
@RestController
@RequestMapping("/api/v1/persons/{person_id}/email")
public class EmailController {
    @Autowired
    private EmailRepository emailRepository;
    @PostMapping("/")
    @ResponseBody
    public EmailModel addEmail(@Valid @RequestBody EmailModel emailModel)
    {
        emailRepository.save(emailModel);
        return emailModel;
    }

    @GetMapping("/")
    @ResponseBody
    public Iterable<EmailModel> viewEmail()
    {
        return emailRepository.findAll();
    }

    @GetMapping("/{email_id}")
    @ResponseBody
    public EmailModel viewByEmailId(@PathVariable Integer email_id)
    {
        Optional<EmailModel> opt = emailRepository.findById(email_id);
        return opt.orElseThrow(() -> new PersonneIntrouvableException("l'emailId = " + email_id));
    }

    @DeleteMapping("/{email_id}")
    @ResponseBody
    public ResponseEntity<Object> deleteEmail(@Valid @PathVariable Integer email_id)
    {
        emailRepository.deleteById(email_id);
        return ResponseEntity.noContent().build();
    }

    @PutMapping("/{email_id}")
    @ResponseBody
    public ResponseEntity<Object> editEmail(@Valid @RequestBody EmailModel emailModel, @PathVariable Integer email_id) {
        Optional<EmailModel> rep = emailRepository.findById(email_id);
        return rep.map( p -> {
            emailModel.setEmail_id(email_id);
            emailRepository.save(emailModel);
            return ResponseEntity.status(HttpStatus.CREATED).build();}
        )
                .orElse(ResponseEntity.notFound().build());}
}







