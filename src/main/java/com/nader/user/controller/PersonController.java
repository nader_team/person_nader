package com.nader.user.controller;

import com.nader.user.exception.PersonneIntrouvableException;
import com.nader.user.model.PersonModel;
import com.nader.user.repository.PersonsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
@RestController
@RequestMapping("/api/v1/persons/")
public class PersonController {
    @Autowired
    private PersonsRepository personsRepository;
    @Autowired
    private PasswordEncoder passwordEncoder;

    @PostMapping("/")
    @ResponseBody
    public PersonModel addPerson(@Valid @RequestBody PersonModel personModel)
    {
        personModel.setPassword(passwordEncoder.encode("password"));
        personsRepository.save(personModel);
        return personModel;
    }

    @GetMapping("/")
    @ResponseBody
    public Iterable<PersonModel> viewPerson()
    {
        return personsRepository.findAll();
    }

    @GetMapping("/{person_id}")
    @ResponseBody
    public PersonModel viewByIdPerson(@PathVariable Integer person_id)
    {
        Optional<PersonModel> opt = personsRepository.findById(person_id);
        return opt.orElseThrow(() -> new PersonneIntrouvableException("l'id = " + person_id));
    }

    @DeleteMapping("/{person_id}")
    @ResponseBody
    public ResponseEntity<Object> deletePerson(@Valid @PathVariable Integer person_id)
    {
        personsRepository.deleteById(person_id);
        return ResponseEntity.noContent().build();
    }

    @GetMapping(value = "firstName/{firstName}")
    @ResponseBody
    public List<PersonModel> getByFirstName(@PathVariable String firstName)
    {
        return personsRepository.getByFirstName(firstName);
    }
    @GetMapping(value = "userName/{userName}")
    @ResponseBody
    public List<PersonModel> getByUserName(@PathVariable String userName)
    {
        return personsRepository.getByUserName(userName);
    }

    @PutMapping("/{person_id}")
    @ResponseBody
    public ResponseEntity<Object> editPerson(@Valid @RequestBody PersonModel personModel, @PathVariable Integer person_id) {
        Optional<PersonModel> rep = personsRepository.findById(person_id);
        return rep.map( p -> {
            personModel.setPassword(passwordEncoder.encode("password"));
            personModel.setPerson_id(person_id);
            personsRepository.save(personModel);
            return ResponseEntity.status(HttpStatus.CREATED).build();}
        )
                .orElse(ResponseEntity.notFound().build());}


}



